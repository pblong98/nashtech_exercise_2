import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http'
import {GLOBAL_SETTING} from '../common/GLOBAL_SETTING'

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  register(email, pass){
    return this.http
      .post(GLOBAL_SETTING.apiURL+"/api/Login/Register", {email: email, password: pass});
  }

  login(email, pass){
    return this.http.post(GLOBAL_SETTING.apiURL+"/api/Login/Login", {email: email, password: pass});
  }

  getAccountInfor(){
    return this.http.get(GLOBAL_SETTING.apiURL+"/api/Login/GetAccountInfor");
  }

  // getAccountInfor(){
  //   var tokenHeader = new HttpHeaders({
  //     'Authorization':'Bearer ' + localStorage.getItem("token")
  //   })
  //   return this.http.get(GLOBAL_SETTING.apiURL+"/api/Login/GetAccountInfor", {headers: tokenHeader});
  // }
}
