import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http'
import {GLOBAL_SETTING} from '../common/GLOBAL_SETTING'

@Injectable({
  providedIn: 'root'
})

export class BlogService {

  constructor(private http: HttpClient) { }

  getDetailBlog(blogId,commentPage){
    return this.http
      .get(GLOBAL_SETTING.apiURL+"/api/Blog/Detail",{params:{BlogId:blogId, CommentPage:commentPage}});
  }

  createComment(blogId,comment){
    return this.http
      .post(GLOBAL_SETTING.apiURL+"/api/Blog/CreateComment",{blogId:blogId, comment:comment});
  }

  deleteComment(commentId){
    return this.http
      .delete(GLOBAL_SETTING.apiURL+"/api/Blog/DeleteComment?commentId="+commentId);
  }

  deleteBlog(blogId){
    return this.http
      .delete(GLOBAL_SETTING.apiURL+"/api/Blog/Delete?blogId="+blogId);
  }

  createPost(title, desc, file){
    const fd = new FormData();
    fd.append('file', file)
    fd.append("name",title)
    fd.append("description",desc)

    return this.http
      .post(GLOBAL_SETTING.apiURL+"/api/Blog/Create",fd);
  }

  // createPost(title, desc, file){
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Authorization':'Bearer ' + localStorage.getItem("token")
  //     })
  //   };

  //   const fd = new FormData();
  //   fd.append('file', file)
  //   fd.append("name",title)
  //   fd.append("description",desc)

  //   return this.http
  //     .post(GLOBAL_SETTING.apiURL+"/api/Blog/Create",fd, httpOptions);
  // }
}
