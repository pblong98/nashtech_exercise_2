import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http'
import {GLOBAL_SETTING} from '../common/GLOBAL_SETTING'

@Injectable({
  providedIn: 'root'
})

export class HomeService {

constructor(private http: HttpClient) { }

  getPostList(page){
    return this.http
      .get(GLOBAL_SETTING.apiURL+"/api/Home/getBlogList?page="+page);
  }

  getPagingModel(currentPage){
    return this.http
      .get(GLOBAL_SETTING.apiURL+"/api/Home/getPagingModel",{params:{page:currentPage}});
  }

}
