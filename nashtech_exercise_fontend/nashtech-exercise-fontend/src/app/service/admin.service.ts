import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders} from '@angular/common/http'
import {GLOBAL_SETTING} from '../common/GLOBAL_SETTING'

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  index(){
    // var tokenHeader = new HttpHeaders({
    //   'Authorization':'Bearer ' + localStorage.getItem("token")
    // })
    return this.http
      .get(GLOBAL_SETTING.apiURL+"/api/Admin/index");
      // .get(GLOBAL_SETTING.apiURL+"/api/Admin/index", {headers: tokenHeader});
  }
}
