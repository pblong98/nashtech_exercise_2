import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/service/blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})

export class CreatePostComponent implements OnInit {

  selectedFile:File

  constructor(private router: Router, private _blogService: BlogService) { }



  ngOnInit(): void {
  }

  selectFile(file){
    console.log(file.target.files.length)
    if(file.target.files.length > 0)
    {
      this.selectedFile = file.target.files[0]
    }
    else{
      this.selectedFile = null
    }
  }

  createPost(title, desc){
    this._blogService.createPost(title, desc, this.selectedFile).subscribe(resp=>{
      console.log(resp)
      if(resp === true){
        this.router.navigate(['list'])
      }
      else{
        alert("Cannot create new post !");
      }
    });
  }





}
