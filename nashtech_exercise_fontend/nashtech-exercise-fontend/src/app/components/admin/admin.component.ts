import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../service/admin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private router: Router, private adminService: AdminService) { }

  statistical:any

  ngOnInit(): void {
    this.getIndex()
  }

  getIndex(){
    this.adminService.index().subscribe(resp=>{
      // console.log(resp)
      this.statistical = resp
    }, error=>{
      console.log(error)
      if(error.status == 403){
        this.router.navigate(['access-denied'])
      }

      if(error.status == 401){
        this.router.navigate(['login'])
      }
    })
  }

}
