import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Save, Reset } from 'src/app/reducers/account.actions';
import {Router} from '@angular/router';

import { AccountService } from 'src/app/service/account.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  account: any
  static accountShare: any
  constructor(public store: Store, private router: Router, private _accountService: AccountService) {
    if(localStorage.getItem("token")!= null){
      _accountService.getAccountInfor().subscribe(resp=>{
        var data:any = resp
        // console.log(data)
        this.store.dispatch(new Save({
          UserId: data.user.userId,
          UserName: data.user.userName,
          UserRoles: data.user.roles
        }))
      })
    }
    this.account = store.source
    NavbarComponent.accountShare = store.source
  }

  ngOnInit(): void {
    // console.log(this.account)
  }

  logOut(){
    localStorage.removeItem("token")
    this.store.dispatch(new Reset())
    this.router.navigate(['login'])
  }
}
