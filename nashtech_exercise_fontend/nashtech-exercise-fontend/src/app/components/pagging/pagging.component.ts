import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pagging',
  templateUrl: './pagging.component.html',
  styleUrls: ['./pagging.component.scss']
})
export class PaggingComponent implements OnInit {

  @Input() totalPage = 1
  @Input() currentPage = 1
  @Input() isDetail = false
  @Input() detailBlogId

  carretLeftBtnPage = 1
  pageBtns = []
  carretRightBtnPage = 1


  constructor() { }

  ngOnInit(): void {
    // console.log(this.totalPage)
    // console.log(this.currentPage)
    this.setUpUI()
    // console.log(this.carretLeftBtnPage)
    // console.log(this.pageBtns)
    // console.log(this.carretRightBtnPage)
  }

  ngOnChanges(changes: PaggingComponent) {
    this.setUpUI()
  }

  setUpUI(){
    this.pageBtns= []

    if(this.currentPage == 0)
      this.currentPage = 1
    ///////////////////////////////////////

    if ((this.currentPage - 1) >= 1)
    {
      this.carretLeftBtnPage = this.currentPage - 1
    }
    else
    {
      this.carretLeftBtnPage = 1
    }

    ///////////////////////////////////////

    var p = this.currentPage;

    if (this.totalPage >= 7)
    {
      for (var i = p - 3; i <= p + 3; i++)
      {
        if (p + 3 > this.totalPage)
        {
          p--;
          i -= 2;
          continue;
        }
        if (i <= 0)
        {
          p++;
        }
        else
        {
          this.pageBtns.push(i)
        }
      }
    }
    else
    {
      for (var j = 1; j <= this.totalPage; j++)
      {
        this.pageBtns.push(j)
      }
    }

    ///////////////////////////////////////

    if(this.currentPage + 1 <= this.totalPage)
    {
      this.carretRightBtnPage = this.currentPage + 1
    }
    else
    {
      this.carretRightBtnPage = this.totalPage
    }
  }
}
