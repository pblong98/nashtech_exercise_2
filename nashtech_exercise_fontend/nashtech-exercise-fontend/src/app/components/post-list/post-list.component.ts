import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/service/home.service';
import {GLOBAL_SETTING} from '../../common/GLOBAL_SETTING';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private _homeService: HomeService) { }

  postList:any
  apiStaticFileURL = GLOBAL_SETTING.apiStaticFileURL
  pagingModel:any
  page = 0

  ngOnInit(): void {
    this.route.queryParams.subscribe(p => {
      console.log(p)
      this.init()
    });
  }

  init(){
    var queryParams:any = this.route.queryParams

    if(queryParams._value.page){
      this.page = queryParams._value.page
    }
    // console.log("page = " + this.page)

    this._homeService.getPostList(this.page).subscribe(resp=>{
      // console.log(resp)
      this.postList = resp
    })

    this._homeService.getPagingModel(this.page).subscribe(resp=>{
      this.pagingModel = resp
      // console.log(this.pagingModel)
    })
  }

}
