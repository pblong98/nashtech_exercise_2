import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { BlogService } from 'src/app/service/blog.service';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import {GLOBAL_SETTING} from '../../common/GLOBAL_SETTING'

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})

export class PostDetailComponent implements OnInit {

  @ViewChild('commentInput') commentInput

  id: number
  commentPage: number
  apiStaticFileURL = GLOBAL_SETTING.apiStaticFileURL
  detailPost: any

  constructor(private route: ActivatedRoute, private router: Router, private _blogService: BlogService) { }

  ngOnInit(): void {
    this.init()
    this.route.queryParams.subscribe(resp=>{
      this.init()
    })
  }

  init(){
    // console.log(this.isLogined())
    this.getData()
  }

  isLogined(){
    var islogined = false;
    if(NavbarComponent.accountShare != null){
      islogined = NavbarComponent.accountShare._value.account.UserId !== ""
    }
    return islogined
  }

  getData(){
    var queryParams:any = this.route.queryParams
    this.id = queryParams._value.id
    this.commentPage = queryParams._value.commentPage

    this._blogService.getDetailBlog(this.id, this.commentPage).subscribe(resp=>{
      this.detailPost = resp
      console.log(this.detailPost)
      if(this.commentPage && (this.detailPost.totalPage < this.commentPage)){
        this.router.navigate(['/detail'],{queryParams: {commentPage:this.detailPost.totalPage, id: this.id}})
      }
    })
  }

  createComment(comment){
    comment = comment.trim()
    if(comment == null || comment.length == 0){
      return
    }
    this._blogService.createComment(this.detailPost.blogDetail.blogId, comment).subscribe(resp=>{
      if(resp === true){
        this.getData()
      }
      else{
        alert("Cannot post comment now !")
      }
    })
    this.commentInput.nativeElement.value = ""
    // console.log(this.commentInput.nativeElement.value)
  }

  deleteComment(commentId){
    // console.log(commentId)
    this._blogService.deleteComment(commentId).subscribe(resp=>{
      // console.log(resp)
      if(resp === true){
        this.getData()
      }
      else{
        alert("Cannot delete comment now !")
      }
    })
  }

  canIDeleteThisComment(commentAuthorId){
    return (commentAuthorId == NavbarComponent.accountShare._value.account.UserId) || (
      NavbarComponent.accountShare._value.account.UserId == this.detailPost.blogDetail.authorId)
  }

  canIDeleteBlog(){
    return NavbarComponent.accountShare._value.account.UserId == this.detailPost.blogDetail.authorId
  }

  deleteBlog(blogId){
    this._blogService.deleteBlog(blogId).subscribe(resp=>{
      if(resp === true){
        this.router.navigate(['list'])
      }
      else{
        alert("Cannot delete this post now !")
      }
    })
  }
}
