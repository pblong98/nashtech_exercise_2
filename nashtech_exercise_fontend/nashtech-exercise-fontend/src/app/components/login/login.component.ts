import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/service/account.service';
import {Router} from '@angular/router';
import { Store } from '@ngrx/store';
import { Save } from 'src/app/reducers/account.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private _accountService: AccountService, private store: Store<any>) { }

  ngOnInit(): void {
  }

  loginStatus = ''

  login(email, pass){
    this._accountService.login(email, pass).subscribe(resp => {

      var data:any = resp
      console.log(data)

      localStorage.setItem("token", data.token)

      this.store.dispatch(new Save({
        UserId: data.user.userId,
        UserName: data.user.userName,
        UserRoles: data.user.roles
      }))

      this.loginStatus = "Logged in successfully ! You will be redirect within 3s"

      this.router.navigate(['list'])

    }, error=>{

      var data:any = error
      console.log(data)
      if(data.status == 400){
        this.loginStatus = "User account or password is incorrect !"
      }
      else{
        this.loginStatus = "Server error !"
      }
    })
  }

}
