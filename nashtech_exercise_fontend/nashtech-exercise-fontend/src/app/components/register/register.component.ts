import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/service/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, private _accountService: AccountService) { }


  ngOnInit(): void {
  }

  registerError = []

  register(email, pass1, pass2){
    this.registerError = []
    if(pass1 !== pass2){
      this.registerError = [{description: "Your confirm password is not match with password"}]
      return
    }
    this._accountService.register(email, pass1).subscribe(resp => {
      var data:any = resp
      console.log(data)
      if(data.succeeded === false){
        this.registerError = data.errors
      }
      else{
        this.registerError = [{description: "Sign up success ! You will be redirected within 3 seconds"}]
        setTimeout(() => {
          this.router.navigate(['login'])
        }, 3000);
      }
    })
  }
}
