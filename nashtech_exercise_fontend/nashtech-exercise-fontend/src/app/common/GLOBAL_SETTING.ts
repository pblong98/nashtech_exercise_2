export class GLOBAL_SETTING {
  public static apiURL: string = "http://localhost:7798";
  public static apiStaticFileURL: string = GLOBAL_SETTING.apiURL+"/StaticFiles";
}
