import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { PaggingComponent } from './components/pagging/pagging.component';
import { PFooterComponent } from './components/p-footer/p-footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { AccountService } from './service/account.service'
import { HomeService } from './service/home.service';
import { AdminService } from './service/admin.service'

import { StoreModule } from '@ngrx/store'
import { accountReducer } from 'src/app/reducers/account.reducer';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { AdminComponent } from './components/admin/admin.component';
import { from } from 'rxjs';
import { AccessDeniedComponent } from './components/access-denied/access-denied.component';
import { AuthInterceptor } from './auth/auth.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PostListComponent,
    PostDetailComponent,
    PaggingComponent,
    PFooterComponent,
    LoginComponent,
    RegisterComponent,
    CreatePostComponent,
    AdminComponent,
    AccessDeniedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({account: accountReducer})
  ],
  providers: [
    AccountService,
    HomeService,
    AdminService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
