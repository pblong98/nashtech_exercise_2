import { Action } from '@ngrx/store';

export enum ActionTypes{
  Save = '[Account] Save',
  Reset = '[Account] Reset'
}

export class Save implements Action{
  readonly type = ActionTypes.Save
  constructor (public payload: any){}
}

export class Reset implements Action{
  readonly type = ActionTypes.Reset
}
