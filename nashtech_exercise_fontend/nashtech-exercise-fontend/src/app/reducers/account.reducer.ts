import {Action} from '@ngrx/store'
import {ActionTypes} from './account.actions'

export const initialState = {
  UserId: "",
  UserName: "",
  UserRoles: []
};

export function accountReducer(state = initialState, action: Action){
  switch(action.type)
  {
    case ActionTypes.Save:
      return state = (<any>action).payload
    case ActionTypes.Reset:
      return state = {
        UserId: "",
        UserName: "",
        UserRoles: []
      };
    default:
      return state;
  }
}
