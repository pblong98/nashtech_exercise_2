﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace nashtech_exercise.Data.Migrations
{
    public partial class changeCommentTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Comment");

            migrationBuilder.AddColumn<string>(
                name: "AuthorIdId",
                table: "Comment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Comment_AuthorIdId",
                table: "Comment",
                column: "AuthorIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_AspNetUsers_AuthorIdId",
                table: "Comment",
                column: "AuthorIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_AspNetUsers_AuthorIdId",
                table: "Comment");

            migrationBuilder.DropIndex(
                name: "IX_Comment_AuthorIdId",
                table: "Comment");

            migrationBuilder.DropColumn(
                name: "AuthorIdId",
                table: "Comment");

            migrationBuilder.AddColumn<string>(
                name: "AuthorId",
                table: "Comment",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
