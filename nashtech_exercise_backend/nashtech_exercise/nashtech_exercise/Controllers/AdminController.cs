﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using nashtech_exercise.Data;

namespace nashtech_exercise.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private readonly ILogger<BlogController> _logger;
        private readonly ApplicationDbContext _context;

        public AdminController(ILogger<BlogController> logger, ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            //var claims = User.Claims.Select(claim => new { claim.Type, claim.Value }).ToArray();

            ViewBag.totalBlog = _context.Blog.Count();
            ViewBag.totalComment = _context.Comment.Count();
            ViewBag.totalUser = _context.Users.Count();
            return View();
        }
    }
}