﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;

namespace nashtech_exercise.Controllers
{
    public class BlogController : Controller
    {
        private readonly ILogger<BlogController> _logger;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly BlogRepo blogRepo;

        public BlogController(ILogger<BlogController> logger, ApplicationDbContext _context, UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            blogRepo = new BlogRepo(_context);
            _userManager = userManager;
        }

        public IActionResult Detail(long blogId, int? commentPage)
        {
            if (commentPage == null || commentPage <= 0)
            {
                commentPage = 1;
            }
            int totalPage = 1;
            var blog = blogRepo.Detail(blogId, commentPage.Value, 5 ,ref totalPage);
            ViewBag.CommentPage = commentPage;
            ViewBag.TotalPage = totalPage / (5 + 1) + 1;
            return View(blog);
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        public IActionResult Delete(long blogId)
        {
            blogRepo.Delete(blogId, _userManager.GetUserId(User));
            return Redirect("/");
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create( BlogModel blog, List<IFormFile> files)
        {
            //UserManager
            if (ModelState.IsValid)
            {
                bool isOk = blogRepo.Create(blog, _userManager.GetUserId(User), files);

                if (isOk)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    //return RedirectToAction(error);
                    //return null;
                }
                return Redirect("/");
            }
            else
            {
                return View(blog);
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateComment(long blogId, string comment)
        {
            blogRepo.CreateComment(blogId, comment, _userManager.GetUserId(User));
            return RedirectToAction("Detail", "Blog", new { blogId = blogId });
        }

        [Authorize]
        public IActionResult DeleteComment(long blogId, long commentId)
        {
            blogRepo.DeleteComment(commentId, _userManager.GetUserId(User));
            return RedirectToAction("Detail", "Blog", new { blogId = blogId });
        }


    }
}