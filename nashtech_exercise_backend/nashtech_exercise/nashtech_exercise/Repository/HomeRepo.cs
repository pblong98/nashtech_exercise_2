﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using nashtech_exercise.Controllers;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_exercise.Repository.Home
{
    public class HomeRepo
    {
        private readonly ApplicationDbContext _context;

        public HomeRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<BlogModel> GetList(int page, int pageSize)
        {
            BlogRepo blogRepo = new BlogRepo(_context);

            if(page <= 0)
            {
                page = 1;
            }

            if(pageSize <= 5)
            {
                pageSize = 5;
            }

            int skip = 0, take = pageSize;

            take = pageSize;
            skip = pageSize * page - pageSize;  

            List<BlogModel> result = new List<BlogModel>();

            List<Entities.Blog> queryResult;

            queryResult = _context.Blog
                .Include(e => e.AuthorId)
                .Include(e => e.Comments)
                .OrderByDescending(blog => blog.CreationDate)
                .Skip(skip)
                .Take(take).ToList();

            //Mapping from entities list to model list
            foreach(var i in queryResult)
            {
                string imgUrl = "";
                if (!string.IsNullOrEmpty(i.ImgUrl))
                {
                    imgUrl = "/upload/thumb_" + i.ImgUrl.Substring(8);
                }
                var authorName = "";
                int commentCount = 0;

                if (i.Comments != null)
                {
                    commentCount = i.Comments.Count;
                }

                if(i.AuthorId != null)
                {
                    authorName = i.AuthorId.UserName;
                }

                result.Add(new BlogModel
                {
                    Name = i.Name,
                    AuthorId = authorName,
                    BlogId = i.BlogId,
                    Description = i.Description,
                    CreationDate = i.CreationDate,
                    ImgUrl = imgUrl,
                    totalComment = commentCount
                }) ;
            }

            return result;
        }
    }
}
