﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using nashtech_exercise.Data;
using nashtech_exercise.Entities;
using nashtech_exercise.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace nashtech_exercise.Repository
{
    public class BlogRepo
    {
        private readonly ApplicationDbContext _context;

        public BlogRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public BlogModel Detail (long blogId, int commentPage, int pageSize, ref int commentTotal)
        {
            try
            {
                var result = _context.Blog
                .Where(b => b.BlogId == blogId)
                .Include(b => b.AuthorId)
                .FirstOrDefault();

                int _commentTotal = _context.Comment
                    .Where(c => c.Blog == result)
                    .Count();
                commentTotal = _commentTotal;

                if (commentPage <= 0)
                {
                    commentPage = 1;
                }

                if (commentPage > _commentTotal)
                {
                    commentPage = _commentTotal;
                }

                if (pageSize <= 5)
                {
                    pageSize = 5;
                }

                int skip = 0, take = pageSize;

                take = pageSize;
                skip = pageSize * commentPage - pageSize;

                List<Comment> _comments = null;
                if (_commentTotal > 0)
                {
                    _comments = _context.Comment
                    .Where(c => c.Blog == result)
                    .OrderByDescending(c => c.CreationDate)
                    .Skip(skip)
                    .Take(take)
                    .Include(c => c.AuthorId)
                    .ToList();
                }

                string authorName = "", authorId = "";
                if (result.AuthorId != null)
                {
                    authorName = result.AuthorId.UserName;
                    authorId = result.AuthorId.Id;
                }

                List<CommentModel> comments = new List<CommentModel>();
                if (_comments != null)
                {
                    foreach (var i in _comments)
                    {
                        string commentAuthorName = "", commentAuthorId = "";
                        if (i.AuthorId != null)
                        {
                            commentAuthorName = i.AuthorId.UserName;
                            commentAuthorId = i.AuthorId.Id;
                        }
                        comments.Add(new CommentModel
                        {
                            AuthorName = commentAuthorName,
                            CommentId = i.CommentId,
                            Content = i.Content,
                            CreationDate = i.CreationDate,
                            AuthorId = commentAuthorId
                        });
                    }
                }

                return new BlogModel
                {
                    Description = result.Description,
                    Name = result.Name,
                    CreationDate = result.CreationDate,
                    ImgUrl = result.ImgUrl,
                    AuthorName = authorName,
                    BlogId = result.BlogId,
                    Comments = comments,
                    AuthorId = authorId
                };
            }
            catch
            {
                return null;
            }
        }

        public bool Create(BlogModel blog, string authorId, List<IFormFile> files)
        {
            if(string.IsNullOrEmpty(blog.Name) || string.IsNullOrEmpty(blog.Description))
            {
                return false;
            }

            try
            {
                string imgUrl = "";
                if (files != null && files.Count > 0 && files[0] != null)
                {
                    imgUrl = UploadImg(files[0]);
                    imgUrl = imgUrl.Substring(imgUrl.IndexOf("/Upload"));
                }

                Blog newBlog = new Blog
                {
                    ImgUrl = imgUrl,
                    CreationDate = DateTime.Now,
                    AuthorId = _context.Users.Find(authorId),
                    Description = blog.Description,
                    Name = blog.Name
                };

                var newb = _context.Blog.Add(newBlog);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public PagingModel GetPagingModel(int currentPage, int pageSize)
        {
            if (currentPage <= 0)
            {
                currentPage = 1;
            }

            if (pageSize <= 5)
            {
                pageSize = 5;
            }

            int totalPage = _context.Blog.Count() / (pageSize + 1) + 1;

            return new PagingModel
            {
                CurrentPage = currentPage,
                TotalPage = totalPage
            };
        }

        public bool Delete(long blogId, string currentUserId)
        {
            var blog = _context.Blog
                .Where(b=>b.BlogId == blogId)
                .Include(b=>b.Comments)
                .Include(b => b.AuthorId)
                .FirstOrDefault();
            if(blog != null)
            {
                if(blog.AuthorId.Id == currentUserId)
                {
                    _context.Comment.RemoveRange(blog.Comments);
                    _context.Blog.Remove(blog);
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public bool CreateComment(long blogId, string comment, string authorId)
        {
            if (string.IsNullOrWhiteSpace(comment))
            {
                return false;
            }
            try
            {
                IdentityUser currentUser = _context.Users.Find(authorId);
                Blog currentBlog = _context.Blog.Find(blogId);
                _context.Comment.Add(new Comment
                {
                    AuthorId = currentUser,
                    Blog = currentBlog,
                    Content = comment,
                    CreationDate = DateTime.Now
                });
                _context.SaveChanges();
                return true;
            }
            catch {
                return false;
            }
        }

        public bool DeleteComment(long commentId, string currentUserId)
        {
            var comment = _context.Comment
                .Where(c=>c.CommentId == commentId)
                .Include(c=>c.AuthorId)
                .Include(c=>c.Blog)
                    .ThenInclude(a=>a.AuthorId)
                .FirstOrDefault();

            if(comment != null)
            {
                if (comment.AuthorId.Id == currentUserId || comment.Blog.AuthorId.Id == currentUserId)
                {
                    _context.Comment.Remove(comment);
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }
        //'D:\Exercise\nashtech_exercise_2\nashtech_exercise_backend\nashtech_exercise\nashtech_execise_api\wwwroot\
        private string UploadImg(IFormFile file)
        {
            string Url = Directory.GetCurrentDirectory();
            Url = Url.Replace(@"nashtech_execise_api", @"nashtech_exercise");


            var guiId = Guid.NewGuid().ToString();

            string filePath = "/wwwroot/Upload/" + guiId + file.FileName;
            filePath = Url+filePath.Remove(filePath.LastIndexOf("."));

            string fileThumbPath = "/wwwroot/Upload/thumb_" + guiId + file.FileName;
            fileThumbPath = Url+fileThumbPath.Remove(fileThumbPath.LastIndexOf("."));

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            using (var stream = new FileStream(fileThumbPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            Image_resize(filePath, filePath + ".jpg", 700);
            Image_resize(fileThumbPath, fileThumbPath + ".jpg", 220);

            System.IO.File.Delete(filePath);
            System.IO.File.Delete(fileThumbPath);

            return filePath.Replace("wwwroot", "") + ".jpg";
        }

        private void Image_resize(string input_Image_Path, string output_Image_Path, int new_Width)
        {
            const long quality = 100L;
            Bitmap source_Bitmap = new Bitmap(input_Image_Path);
            double dblWidth_origial = source_Bitmap.Width;
            double dblHeigth_origial = source_Bitmap.Height;
            double relation_heigth_width = dblHeigth_origial / dblWidth_origial;
            int new_Height = (int)(new_Width * relation_heigth_width);

            //< create Empty Drawarea >
            var new_DrawArea = new Bitmap(new_Width, new_Height);

            //</ create Empty Drawarea >
            using (var graphic_of_DrawArea = Graphics.FromImage(new_DrawArea))
            {
                //< setup >
                graphic_of_DrawArea.CompositingQuality = CompositingQuality.HighSpeed;
                graphic_of_DrawArea.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic_of_DrawArea.CompositingMode = CompositingMode.SourceCopy;
                //</ setup >
                //< draw into placeholder >
                //*imports the image into the drawarea
                graphic_of_DrawArea.DrawImage(source_Bitmap, 0, 0, new_Width, new_Height);
                //</ draw into placeholder >
                //--< Output as .Jpg >--
                using (var output = System.IO.File.Open(output_Image_Path, FileMode.Create))
                {
                    //< setup jpg >
                    var qualityParamId = Encoder.Quality;
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                    //</ setup jpg >
                    //< save Bitmap as Jpg >
                    var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);
                    new_DrawArea.Save(output, codec, encoderParameters);
                    //resized_Bitmap.Dispose();
                    output.Close();
                    //</ save Bitmap as Jpg >
                }
                //--</ Output as .Jpg >--
                graphic_of_DrawArea.Dispose();
            }
            source_Bitmap.Dispose();
        }
    }
}
