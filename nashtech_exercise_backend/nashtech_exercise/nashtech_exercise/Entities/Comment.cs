﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_exercise.Entities
{
    public class Comment
    {
        [Key]
        public long CommentId { get; set; }
        public Blog Blog { get; set; }
        public IdentityUser AuthorId { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Content { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }
    }
}
