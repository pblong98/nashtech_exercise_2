﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using nashtech_exercise.Data;

namespace nashtech_execise_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<IdentityUser> UserManager;

        public AdminController(ApplicationDbContext context, UserManager<IdentityUser> UserManager)
        {
            this.context = context;
            this.UserManager = UserManager;
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public object Index()
        {
            var claims = User.Claims.Select(claim => new { claim.Type, claim.Value }).ToArray();

            return new
            {
                totalBlog = context.Blog.Count(),
                totalComment = context.Comment.Count(),
                totalUser = context.Users.Count()
            };
        }
    }
}