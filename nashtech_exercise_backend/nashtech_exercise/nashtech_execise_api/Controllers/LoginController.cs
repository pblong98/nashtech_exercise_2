﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using nashtech_execise_api.ApiModel;
using nashtech_execise_api.ViewModel;
using nashtech_exercise.Data;

namespace nashtech_execise_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext context;
        private readonly IConfiguration config;

        public LoginController(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext _context,
            IConfiguration _config
            )
        {
            _signInManager = signInManager;
            _userManager = userManager;
            context = _context;
            config = _config;
        }

        [HttpPost]
        public IActionResult Login(LoginRegiterModel model)
        {
            var result = _signInManager.PasswordSignInAsync(model.email, model.password, true, false).Result;
            var user = context.Users.Where(u => u.UserName == model.email).FirstOrDefault();
            

            if (result.Succeeded)
            {
                var roles = context.UserRoles.Where(ur => ur.UserId == user.Id)
                .Join(context.Roles,
                    userrole => userrole.RoleId,
                    role => role.Id,
                    (userrole, role) => new
                    {
                        role = role.Name
                    }
                ).Select(x => x.role).ToList();

                //Assigned role to the user
                var role = _userManager.GetRolesAsync(user).Result;
                IdentityOptions _options = new IdentityOptions();

                var claimsIdentity = new ClaimsIdentity();
                if (role.Count == 0)
                {
                    claimsIdentity = new ClaimsIdentity(new Claim[] {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                    });
                }
                else
                {
                    claimsIdentity = new ClaimsIdentity(new Claim[] {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    });
                }

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = claimsIdentity,
                    Expires = DateTime.UtcNow.AddDays(3),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["ApplicationSetting:JWT_Secret"])), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { 
                    token,
                    User = new UserApiModel
                    {
                        UserId = user.Id,
                        UserName = user.UserName,
                        Roles = roles
                    }
                });
            }
            else
            {
                return BadRequest(new { Status = "FAIL" });
            }
        }

        [HttpPost]
        public IdentityResult Register(LoginRegiterModel loginRegiterModel)
        {
            IdentityResult result = null;
            if (loginRegiterModel != null)
            {
                result = _userManager.CreateAsync(new IdentityUser { UserName = loginRegiterModel.email, Email = loginRegiterModel.email }, loginRegiterModel.password).Result;
            }
            return result;
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetAccountInfor()
        {
            var user = context.Users.Find(User.Claims.First().Value);
            if (user != null)
            {
                var roles = context.UserRoles.Where(ur => ur.UserId == user.Id)
                .Join(context.Roles,
                    userrole => userrole.RoleId,
                    role => role.Id,
                    (userrole, role) => new
                    {
                        role = role.Name
                    }
                ).Select(x => x.role).ToList();

                return Ok(new
                {
                    User = new UserApiModel
                    {
                        UserId = user.Id,
                        UserName = user.UserName,
                        Roles = roles
                    }
                });
            }
            else
            {
                return BadRequest(new { Status = "FAIL" });
            }
        }
    }
}