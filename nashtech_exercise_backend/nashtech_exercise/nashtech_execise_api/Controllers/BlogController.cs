﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using nashtech_execise_api.ApiModel;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;

namespace nashtech_execise_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly BlogRepo blogRepo;

        public BlogController(ApplicationDbContext _context, UserManager<IdentityUser> userManager)
        {
            blogRepo = new BlogRepo(_context);
            _userManager = userManager;
        }

        [HttpGet]
        public object Detail(long blogId, int? commentPage)
        {
            if (commentPage == null || commentPage <= 0)
            {
                commentPage = 1;
            }
            int totalPage = 1;
            var blog = blogRepo.Detail(blogId, commentPage.Value, 5, ref totalPage);
            return new { 
                blogDetail = blog,
                commentPage = commentPage,
                totalPage = totalPage / (5 + 1) + 1
            };
        }

        [HttpPost]
        [Authorize]
        public bool Create([FromForm]string name, [FromForm]string description, IFormFile file)
        {
            BlogModel blog = new BlogModel
            {
                Description = description,
                Name = name
            };
            List<IFormFile> files = new List<IFormFile>();
            files.Add(file);
            return blogRepo.Create(blog, User.Claims.First().Value, files);
        }

        [HttpDelete]
        [Authorize]
        public bool Delete(long blogId)
        {
            return blogRepo.Delete(blogId, User.Claims.First().Value);
        }

        [HttpPost]
        [Authorize]
        public bool CreateComment(CreateModel createModel)
        {
            return blogRepo.CreateComment(createModel.BlogId, createModel.Comment, User.Claims.First().Value);
        }

        [HttpDelete]
        [Authorize]
        public bool DeleteComment(long commentId)
        {
            return blogRepo.DeleteComment(commentId, User.Claims.First().Value);
        }
    }
}