﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;
using nashtech_exercise.Repository.Home;

namespace nashtech_execise_api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly HomeRepo homeRepo;
        private readonly BlogRepo blogRepo;

        public HomeController(ApplicationDbContext _context)
        {
            homeRepo = new HomeRepo(_context);
            blogRepo = new BlogRepo(_context);
        }
        
        [HttpGet]
        public string Index()
        {
            return "home api";
        }

        [HttpGet]
        public List<BlogModel> getBlogList(int page = 1)
        {
            if (page <= 0)
            {
                page = 1;
            }
            return homeRepo.GetList(page, 8);
        }

        [HttpGet]
        public PagingModel getPagingModel(int page = 1)
        {
            return blogRepo.GetPagingModel(page, 8);
        }
    }
}