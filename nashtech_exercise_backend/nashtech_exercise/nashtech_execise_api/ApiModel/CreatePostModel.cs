﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_execise_api.ApiModel
{
    public class CreatePostModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
