﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_execise_api.ApiModel
{
    public class CreateModel
    {
        public long BlogId { get; set; }
        public string Comment { get; set; }
    }
}
