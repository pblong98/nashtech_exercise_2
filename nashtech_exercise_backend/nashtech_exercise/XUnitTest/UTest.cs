using Microsoft.EntityFrameworkCore;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;
using nashtech_exercise.Repository.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UTest
    {

        ApplicationDbContext context;
        BlogRepo blogRepo;
        HomeRepo homeRepo;

        public UTest()
        {
            var option = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            context = new ApplicationDbContext(option);
            blogRepo = new BlogRepo(context);
            homeRepo = new HomeRepo(context);
        }

        [Fact]
        public void T_CreatePost_ReturnTrue()
        {
            var newBlogModel = new BlogModel
            {
                Name = "new blog",
                CreationDate = DateTime.Now,
                Description = "Blog description",
            };
            blogRepo.Create(newBlogModel, null, null);

            var result = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            Assert.True(result.Name == newBlogModel.Name);
        }

        [Fact]
        public void T_CreatePostWithoutTitle_ReturnFalse()
        {
            var newBlogModel = new BlogModel
            {
                Name = "",
                CreationDate = DateTime.Now,
                Description = "Blog description",
            };
            var result = blogRepo.Create(newBlogModel, null, null);
            Assert.False(result);
        }

        [Fact]
        public void T_CreatePostWithoutDescription_ReturnFalse()
        {
            var newBlogModel = new BlogModel
            {
                Name = "Title",
                CreationDate = DateTime.Now,
                Description = "",
            };
            var result = blogRepo.Create(newBlogModel, null, null);
            Assert.False(result);
        }

        [Fact]
        public void T_CreatePostWithoutDescriptionAndTitle_ReturnFalse()
        {
            var newBlogModel = new BlogModel
            {
                Name = "",
                CreationDate = DateTime.Now,
                Description = "",
            };
            var result = blogRepo.Create(newBlogModel, null, null);
            Assert.False(result);
        }

        [Fact]
        public void T_GetDetailPostById_ReturnPost()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };
            blogRepo.Create(newBlogModel, null, null);

            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();
            int totalComment = 0;
            var result = blogRepo.Detail(newBlog.BlogId, 1, 4, ref totalComment);
            Assert.True(result.BlogId == newBlog.BlogId);
        }

        [Fact]
        public void T_GetDetailPostByIdDoNotExisted_ReturnNull()
        {
            int totalComment = 0;
            var result = blogRepo.Detail(0, 1, 4, ref totalComment);
            Assert.Null(result);
        }

        [Fact]
        public void T_DeletePostByOwner_ReturnTrue()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.Delete(newBlog.BlogId, testAuthor.Id);

            var result = context.Blog.Find(newBlog.BlogId);

            Assert.Null(result);
        }

        [Fact]
        public void T_DeletePostNotByOwner_ReturnFalse()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.Delete(newBlog.BlogId, "test2@gmail.com");

            var result = context.Blog.Find(newBlog.BlogId);

            Assert.NotNull(result);
        }

        [Fact]
        public void T_CreateComment_ReturnTrue()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.CreateComment(newBlog.BlogId, "some thing", testAuthor.Id);
            var result = context.Comment.OrderByDescending(b => b.CreationDate).FirstOrDefault();
            Assert.True(result.Content == "some thing");
        }

        [Fact]
        public void T_CreateCommentWithoutContent_ReturnFalse()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.CreateComment(newBlog.BlogId, " ", testAuthor.Id);
            var result = context.Comment.OrderByDescending(b => b.CreationDate).FirstOrDefault();
            Assert.True(result == null);
        }

        [Fact]
        public void T_DeleteCommentByOwner_ReturnTrue()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.CreateComment(newBlog.BlogId, "some thing", testAuthor.Id);
            var newComment = context.Comment.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.DeleteComment(newComment.CommentId, testAuthor.Id);

            var result = context.Comment.Find(newComment.CommentId);

            Assert.Null(result);
        }

        [Fact]
        public void T_DeleteCommentNotByOwner_ReturnTrue()
        {
            var newBlogModel = new BlogModel
            {
                Name = "title",
                CreationDate = DateTime.Now,
                Description = "description",
            };

            context.Users.Add(new Microsoft.AspNetCore.Identity.IdentityUser
            {
                Email = "test@gmail.com",
                PasswordHash = "r3q4feaw3d45t",
                EmailConfirmed = true,
                UserName = "test@gmail.com"
            });
            context.SaveChanges();

            var testAuthor = context.Users.Where(u => u.UserName == "test@gmail.com").FirstOrDefault();

            blogRepo.Create(newBlogModel, testAuthor.Id, null);
            var newBlog = context.Blog.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.CreateComment(newBlog.BlogId, "some thing", testAuthor.Id);
            var newComment = context.Comment.OrderByDescending(b => b.CreationDate).FirstOrDefault();

            blogRepo.DeleteComment(newComment.CommentId, "test2@gmail.com");

            var result = context.Comment.Find(newComment.CommentId);

            Assert.NotNull(result);
        }

        [Fact]
        public void T_GetListBlog()
        {
            for(int i = 1; i< 15; i++)
            {
                var newBlogModel = new BlogModel
                {
                    Name = "new blog " + i,
                    CreationDate = DateTime.Now,
                    Description = "Blog description",
                };
                blogRepo.Create(newBlogModel, null, null);
            }

            var result = homeRepo.GetList(1, 5);
            Assert.IsType<List<BlogModel>>(result);
        }




    }
}
